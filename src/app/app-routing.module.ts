import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent } from './componentes/header/header.component';
import { FooterComponent } from './componentes/footer/footer.component';
import { CatalogoComponent } from './paginas/catalogo/catalogo.component';
import { ProductoComponent } from './paginas/producto/producto.component';
import { CarritoComponent } from './paginas/carrito/carrito.component';

const routes: Routes = [
  {
    path: 'Catalogo/:tag',
    component: CatalogoComponent,
  },
  {
    path: 'Producto/:idP',
    component: ProductoComponent,
  },
  {
    path: 'carrito',
    component: CarritoComponent,
  },
  {
    path: '**',
    redirectTo: 'Catalogo/EQGCV80D',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
