import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Inject } from "@angular/core";
import { DOCUMENT } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html',
  styleUrls: ['./carrito.component.css'],
})
export class CarritoComponent implements OnInit,OnDestroy {
  private baseURL = 'https://orvity.com/orv/';
  public cart = [];
  producto: any = [];
  public total = 0;
  public total2 = '';
  public totalwahts = 0;
  public totalwahts2 = '';
  public tag;
  public envio;
  public precioEnvio:number;
  public precioEnvio1 = '';
  public stringEnvio;
  public direccionActiva = true;
  cantidad: number;
  nombreEmpresa = '';
  nombreProductos = '';
  messenger = '';
  messengerName='';
  img = '';
  direccion = '';
  comentario = '';
  comentarioFinal = '';
  nombre = '';
  nombreFinal = '';
  premium = '';
  enviarlo='Favor de enviarlo a ';
  direccionFinal='';
  imgPortada = "";
  numeroWhats:number;
  @ViewChild("whats", { static: false }) whats: ElementRef;
  @ViewChild("messeger", { static: false }) messeger: ElementRef;
  @ViewChild('openModal', { static: false }) openModal: ElementRef;
  @ViewChild('closeModal', { static: false }) closeModal: ElementRef;
  copyClipboard = '';
  private dom: Document;
  activar_metodo = true;

  constructor(private http: HttpClient, private toastr: ToastrService, @Inject(DOCUMENT) dom: Document, private afs: AngularFirestore) {
    this.dom = dom;
  }
  ngOnDestroy(): void {
    this.abrir(0).unsubscribe();
  }

  ngOnInit() {
    var name = JSON.parse(localStorage.getItem('infoEmpresa'));
    this.nombreEmpresa = '“' + name[0].username+'”';
    this.img = name[0].image;
    this.imgPortada = name[0].imagen_portada;
    this.numeroWhats = name[0].whatsappEmpresa;
    this.messengerName = name[0].telefono_segundo;
    this.envio = name[0].envio_Domicilio;
    this.precioEnvio = parseFloat(name[0].precio_Envio);
    this.precioEnvio1 = (name[0].precio_Envio);
    this.premium = name[0].premium;
    this.tag = JSON.parse(localStorage.getItem('tag'));
    console.log(this.precioEnvio);

    if (JSON.parse(localStorage.getItem('cartdata'))==null) {
      return;
    }else{
      this.cart = JSON.parse(localStorage.getItem('cartdata'));
      console.log('cart', this.cart);

      var previousProducts = JSON.parse(localStorage.getItem('products'));

      for (var i = 0; i < this.cart.length; i++) {
        this.total += this.cart[i].cantidad * this.cart[i].price;
        this.nombreProductos +=
          '-' +
          '%20' +
          this.cart[i].cantidad.toString() +
          '%20“' +
          this.cart[i].title +
          '”%20' +
          '= $' +
          this.cart[i].price +
          '%0A';

        this.messenger +=
          '-' + ' ' + this.cart[i].cantidad.toString() +
          ' “' + this.cart[i].title + '” ' +
          '= $' + this.cart[i].price + '\n';
      }
      this.totalwahts = (this.total + this.precioEnvio);
      this.total2 = Number(this.total).toFixed(2);
      this.totalwahts2 = Number(this.totalwahts).toFixed(2);
    }
 if (this.envio==='0') {
   this.stringEnvio = '';
   this.direccion = 'Lo pasaré a recoger';
   this.direccionFinal=this.direccion;
   this.direccionActiva = false;
 }else{
   if (this.precioEnvio1 === '0.00') {
    this.stringEnvio = '- Envío a domicilio = $'+'Por zona'+'';
    this.totalwahts2 = Number(this.totalwahts).toFixed(2) +' Más Envio';
   } else {
     this.totalwahts2 = Number(this.totalwahts).toFixed(2);
    this.stringEnvio = '- Envío a domicilio = $'+this.precioEnvio1+'';
   }

 }
 this.activar_metodo = true;
  }

  deleteProduct(index) {
    this.cart = JSON.parse(localStorage.getItem('cartdata'));
    console.log('cart', this.cart);

    var previousProducts = JSON.parse(localStorage.getItem('products'));
    console.log('previousProducts', previousProducts);

    // var index = this.cart.findIndex((x) => x.id === previousProducts[0].id);

    console.log('index', index);
    this.cart.splice(index, 1);
    localStorage.setItem('cartdata', JSON.stringify(this.cart));

    this.total = 0;
    this.totalwahts = 0;
    this.nombreProductos = '';
    for (var i = 0; i < this.cart.length; i++) {
      this.total += this.cart[i].cantidad * this.cart[i].price;
      this.nombreProductos +=
        '-' +
        '%20' +
        this.cart[i].cantidad.toString() +
        '%20”' +
        this.cart[i].title +
        '”%20' +
        '= $' +
        this.cart[i].price +
        '%0A';

      this.messenger +=
        '-'+' '+this.cart[i].cantidad.toString()+
        ' ”' +this.cart[i].title +'” '+
        '= $' + this.cart[i].price +'\n';
    }
    this.totalwahts = (this.total + this.precioEnvio);
    this.total2 = Number(this.total).toFixed(2);
    this.totalwahts2 = Number(this.totalwahts).toFixed(2);
  }

  public direccionPedido(event: any): void {
    this.direccionFinal=this.enviarlo+'”'+this.direccion+'”';
    this.copyClipboard = 'Hola ' + this.nombreEmpresa + ', mi nombre es ' + this.nombreFinal + '. Te solicito lo siguiente utilizando ORVITY:\n\n' + this.messenger + this.stringEnvio+ '\n\nTotal: $' + this.totalwahts2 +'\n\n'+ this.direccionFinal + '\n\nComentario: ' + this.comentarioFinal + '\n\nMuchas gracias.';
  }
  public comentarioPedido(event: any): void {
    this.comentarioFinal = '”'+this.comentario+'”';
    //  if (this.activar_metodo === true) {
    //     this.direccionFinal = 'Lo pasaré a recoger';
    //     this.direccion = 'Lo pasaré a recoger';
    //     this.stringEnvio = '';
    //     this.totalwahts2 = String(this.total);
    //   }
    this.copyClipboard = 'Hola ' + this.nombreEmpresa + ', mi nombre es ' + this.nombreFinal + '. Te solicito lo siguiente utilizando ORVITY:\n\n' + this.messenger + this.stringEnvio+ '\n\nTotal: $' + this.totalwahts2 +'\n\n'+ this.direccionFinal + '\n\nComentario: ' + this.comentarioFinal + '\n\nMuchas gracias.';
  }
  public nombrePedido(event: any): void {
    this.nombreFinal = '”'+this.nombre+'”';
  }

  validar() {
    if (this.cart.length==0) {
      this.toastr.error('debes agregar por lo menos un producto al carrito', 'Error');
    }else if (this.nombre=='') {
      this.toastr.error('debes agregar un nombre', 'Error');
    }else if (this.direccion=='') {
      console.log(this.activar_metodo);
      if (this.activar_metodo === true) {
        this.direccionFinal = 'Lo pasaré a recoger';
        this.direccion = 'Lo pasaré a recoger';
        this.stringEnvio = '';
        this.totalwahts2 = String(this.total);
        this.whats.nativeElement.click();
        this.direccion = '';
        this.comentario = '';
        this.nombre = '';
        this.copyClipboard = '';
        localStorage.removeItem('cartdata');
      } else {
        this.toastr.error('debes agregar una dirección', 'Error');
      }
    }else if (this.comentario=='') {
      this.toastr.error('debes agregar un comentario', 'Error');
    }else{
      this.whats.nativeElement.click();
      this.direccion = '';
      this.comentario = '';
      this.nombre = '';
      this.copyClipboard = '';
      localStorage.removeItem('cartdata');
    }
  }

validar2(id){
  if (this.cart.length == 0) {
    this.toastr.error('debes agregar por lo menos un producto al carrito', 'Error');
  } else if (this.nombre == '') {
    this.toastr.error('debes agregar un nombre', 'Error');
  } else if (this.direccion=='') {
      console.log(this.activar_metodo);
      if (this.activar_metodo === true) {
        this.direccionFinal = 'Lo pasaré a recoger';
        this.direccion = 'Lo pasaré a recoger';
        this.stringEnvio = '';
        this.totalwahts2 = String(this.total);
        this.copyClipboard = 'Hola ' + this.nombreEmpresa + ', mi nombre es ' + this.nombreFinal + '. Te solicito lo siguiente utilizando ORVITY:\n\n' + this.messenger + this.stringEnvio+ '\n\nTotal: $' + this.totalwahts2 +'\n\n'+ this.direccionFinal + '\n\nComentario: ' + this.comentarioFinal + '\n\nMuchas gracias.';
      } else {
        this.toastr.error('debes agregar una dirección', 'Error');
      }
    }else if (this.comentario == '') {
    this.toastr.error('debes agregar un comentario', 'Error');
  } else {
    this.copyElementText(id);
  }
}
  copyElementText(id) {
    var element = null; // Should be <textarea> or <input>
    try {
      element = this.dom.getElementById(id);
      element.select();
      this.dom.execCommand("copy");
      this.toastr.success('Pega tu pedido al abrir Messenger', 'Copiado');
    }
    finally {
      this.dom.getSelection().removeAllRanges;
    }
    this.messeger.nativeElement.click();
    this.direccion = '';
    this.comentario = '';
    this.nombre = '';
    this.copyClipboard = '';
    localStorage.removeItem('cartdata');
  }

  confirmaEliminar(i){
    this.toastr.warning('¿Estas seguro de eliminarlo?', 'Toca para confirmar',{
      closeButton:true,
      progressBar:false,
      positionClass: 'toast-center-center',
      disableTimeOut:true
    })
      .onTap.subscribe(() => this.deleteProduct(i));
  }

  addtoCart2(id) {
    if (this.cantidad == 0 || this.cantidad == undefined) {
      this.toastr.error('la cantidad no puede ser 0', 'Error');
      return;
    } else {
      //filtering selected data with incoming data from localstorage using id
      var fetchdata = this.fetchProduct().filter((m) => m.id == id);
      // console.log('Selected Product', fetchdata)
      var previousCartItems =
        JSON.parse(localStorage.getItem('cartdata')) || [];

      // console.log('previousItem',previousCartItems);
      // console.log(fetchdata[0].id);

      var addmore = previousCartItems.filter(
        (d) => d.id == fetchdata[0].id
      );
      var index = previousCartItems.findIndex(
        (d) => d.id == fetchdata[0].id
      );

      // console.log('Matheched',addmore);
      // console.log('INdex',index);
      if (addmore.length > 0) {
        // console.log(addmore.cantidad)
        var cartItem: any = {
          id: fetchdata[0].id,
          title: fetchdata[0].title,
          price: fetchdata[0].price,
          // "qty": addmore.quantity
          cantidad: this.cantidad,
          description: fetchdata[0].description,
          imageProduct: fetchdata[0].imageProduct,
        };
        previousCartItems[index] = cartItem;
        this.toastr.success(
          'Se actualizo la cantidad',
          fetchdata[0].title
        );
      } else {
        this.toastr.success(
          'Se agrego al carrito',
          fetchdata[0].title
        );
        var cartItem: any = {
          id: fetchdata[0].id,
          title: fetchdata[0].title,
          price: fetchdata[0].price,
          // "qty": addmore.quantity
          cantidad: this.cantidad,
          description: fetchdata[0].description,
          imageProduct: fetchdata[0].imageProduct,
        };
        previousCartItems.push(cartItem);
      }
      localStorage.setItem('cartdata', JSON.stringify(previousCartItems));
      console.log(localStorage.getItem('cartdata'));
      this.actualizar();
      this.cantidad=undefined;
      this.closeModal.nativeElement.click();
    }
  }

  fetchProduct() {
    // localStorage.removeItem('cartdata');
    return JSON.parse(localStorage.getItem('products')) || []; //saved data of product into localStorage
  }

  actualizar() {
    this.cart = JSON.parse(localStorage.getItem('cartdata'));
    this.total = 0;
    for (var i = 0; i < this.cart.length; i++) {
      this.total += this.cart[i].cantidad * this.cart[i].price;
    }
    this.total2 = Number(this.total).toFixed(2);
  }

  toggle(e){
    console.log(e)
    if (e === false) {
      this.activar_metodo = true;
      this.total2 = Number(this.total).toFixed(2);
      this.direccion = '';
    } else {
      this.activar_metodo = false;
      this.direccion = '';
      this.total2 = this.totalwahts2;
    }
  }

public cantidadProduct(event: any): void {
    console.log(this.cantidad);
  }

  abrir(idP){
    return this.afs.collection('Product', ref => ref.where('id', '==', `${idP}`)).valueChanges().subscribe(product=>{
      this.producto = product;
    });
    this.openModal.nativeElement.click();
  }


}
