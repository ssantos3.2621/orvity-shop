import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { ProductserviceService } from '../../productservice.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
// import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-catalogo',
  templateUrl: './catalogo.component.html',
  styleUrls: ['./catalogo.component.css'],
})
export class CatalogoComponent implements OnInit,OnDestroy {
  private baseURL = 'https://orvity.com/orv/';
  productos: any = [];
  categorias: any = [];

  category = 0;
  empresa: any ;
  public pproduct = [];
  public filterData: any = [];
  public tag;
  public idEmpresa;
  public cart = [];
  public total = 0;
  public total2 = '';
  public contador = 0;
  cantidad: number = 1;
  nombreEmpresa = '';
  img = '';
  imgPortada = '';
  premium = '';
  producto: any = [];
  @ViewChild('nombre', { static: false }) nombre: ElementRef;
  @ViewChild('closeModal', { static: false }) closeModal: ElementRef;

  constructor(
    public route: ActivatedRoute,
    private toastr: ToastrService,
    private http: HttpClient,
    private productService: ProductserviceService,
    private router: Router,
    private afs: AngularFirestore,
  ) {
    this.route.params.subscribe((parametros) => {
      this.tag = parametros['tag'];
      console.log(this.tag);
      localStorage.setItem('tag', JSON.stringify(this.tag));
    });
  }
  ngOnDestroy(): void {
    this.obtenerProductos().unsubscribe();
    this.obtenerEmpresa().unsubscribe();
    this.modal(0).unsubscribe();
  }

  ngOnInit(): void {
    this.obtenerProductos();
    this.ProductosCategoria();
    this.obtenerEmpresa();
    if (JSON.parse(localStorage.getItem('cartdata')) == null) {
      return;
    } else {
      this.cart = JSON.parse(localStorage.getItem('cartdata'));
      console.log('cart', this.cart);

      for (var i = 0; i < this.cart.length; i++) {
        this.total += this.cart[i].cantidad * this.cart[i].price;
      }
      this.contador = this.cart.length;
     this.total2 = Number(this.total).toFixed(2);
    }

  }

  cartbox = function () {
    var elemento = document.getElementById('cartt');
    if (elemento.classList.contains('cart-open')) {
      elemento.classList.remove('cart-open');
    } else {
      elemento.classList.add('cart-open');
    }
  };

  obtenerProductos(){
    return this.afs.collection('Product', ref => ref.where('tag', '==', `${this.tag}`).orderBy('timestamp', 'desc')).valueChanges().subscribe(products=>{
      console.log("products", products);
      this.productos = products;
      localStorage.setItem('products', JSON.stringify(this.productos));
      this.pproduct = this.productService.fetchProduct();
    });
  }

  ProductosCategoria() {
    this.categorias = this.afs.collection('Categories', ref => ref.where('tag', '==', `${this.tag}`).orderBy('timestamp', 'desc')).valueChanges();
  }

  obtenerEmpresa() {
    return this.afs.collection('Users', ref => ref.where('tag_user', '==', `${this.tag}`)).valueChanges().subscribe(
      user => {
        console.log("empresa", user);
        this.empresa = user;
        this.nombreEmpresa = this.empresa[0].username;
        this.img = this.empresa[0].image;
        this.imgPortada = this.empresa[0].imagen_portada;
        this.idEmpresa = this.empresa[0].id;
        this.premium = this.empresa[0].premium;
        console.log("empresa",this.empresa);
        localStorage.setItem('infoEmpresa', JSON.stringify(this.empresa));
        localStorage.setItem('idEmp', JSON.stringify(this.idEmpresa));
    });
  }

  modal(idP) {
    return this.afs.collection('Product', ref => ref.where('id', '==', `${idP}`)).valueChanges().subscribe(res=>{
      this.producto =res;
    })
  }

  public cantidadProduct(event: any): void {
    console.log(this.cantidad);
  }

  public filtrarCategoria(event: any): void {
    if (this.category == 0) {
      this.obtenerProductos();
    } else {
      const headers: any = new HttpHeaders({
        'Content-Type': 'application/json',
      });
      const options: any = {
        caso: 3,
        idCategoria: this.category,
        tag: this.tag,
      };
      const URL: any = this.baseURL + 'compras.php';
      this.http
        .post(URL, JSON.stringify(options), headers)
        .subscribe((respuesta) => {
          this.productos = respuesta;
          if (this.productos == null) {
            this.productos = [];
            console.log(this.productos);
          }
          console.log(this.productos);
        });
    }
  }

  saveProduct() {
    var previousProducts = JSON.parse(localStorage.getItem('products')) || [];

    previousProducts.push(this.productos);

    localStorage.setItem('products', JSON.stringify(previousProducts));
    this.pproduct = this.productService.fetchProduct();

  }

  onDelete(id) {
    //findIndex(used to find particular index) and slice(slice is used to delete that particulr index)
    var index = this.pproduct.findIndex((x) => x.id === id);
    this.pproduct.splice(index, 1); //(index,1) delete only particular array element according to index
    localStorage.setItem('products', JSON.stringify(this.pproduct));
  }

  addtoCart(id) {
    this.filterData = this.productService.addtoCart(id, this.cantidad);
    // console.log(this.filterData)
    this.cartbox();
    this.actualizar();
  }

  fetchProduct() {
    // localStorage.removeItem('cartdata');
    return JSON.parse(localStorage.getItem('products')) || []; //saved data of product into localStorage
  }

  addtoCart2(id) {
    if (this.cantidad == 0) {
      this.toastr.error('la cantidad no puede ser 0', 'Error');
      return;
    } else {
      //filtering selected data with incoming data from localstorage using id
      var fetchdata = this.fetchProduct().filter((m) => m.id == id);
      // console.log('Selected Product', fetchdata)
      var previousCartItems =
        JSON.parse(localStorage.getItem('cartdata')) || [];

      // console.log('previousItem',previousCartItems);
      // console.log(fetchdata[0].id);

      var addmore = previousCartItems.filter(
        (d) => d.id == fetchdata[0].id
      );
      var index = previousCartItems.findIndex(
        (d) => d.id == fetchdata[0].id
      );

      // console.log('Matheched',addmore);
      // console.log('INdex',index);
      if (addmore.length > 0) {
        // console.log(addmore.cantidad)
        var cartItem: any = {
          id: fetchdata[0].id,
          title: fetchdata[0].title,
          price: fetchdata[0].price,
          // "qty": addmore.quantity
          cantidad: this.cantidad,
          description: fetchdata[0].description,
          imageProduct: fetchdata[0].imageProduct,
        };
        previousCartItems[index] = cartItem;
        this.toastr.success(
          'Se actualizo la cantidad',
          fetchdata[0].title
        );
      } else {
        this.toastr.success(
          'Se agrego al carrito',
          fetchdata[0].title
        );
        var cartItem: any = {
          id: fetchdata[0].id,
          title: fetchdata[0].title,
          price: fetchdata[0].price,
          // "qty": addmore.quantity
          cantidad: this.cantidad,
          description: fetchdata[0].description,
          imageProduct: fetchdata[0].imageProduct,
        };
        previousCartItems.push(cartItem);
      }
      localStorage.setItem('cartdata', JSON.stringify(previousCartItems));
      console.log(localStorage.getItem('cartdata'));
      this.cantidad = 1;
      this.actualizar();
      this.closeModal.nativeElement.click();
      this.cartbox();
    }
  }

  deleteProduct(index) {
    this.cart = JSON.parse(localStorage.getItem('cartdata'));
    console.log('cart', this.cart);

    var previousProducts = JSON.parse(localStorage.getItem('products'));
    console.log('previousProducts', previousProducts);

    // var index = this.cart.findIndex((x) => x.id === previousProducts[0].id);

    console.log('index', index);
    this.cart.splice(index, 1);
    localStorage.setItem('cartdata', JSON.stringify(this.cart));

    this.total = 0;
    for (var i = 0; i < this.cart.length; i++) {
      this.total += this.cart[i].cantidad * this.cart[i].price;
    }
    this.contador = this.cart.length;
    this.total2 = Number(this.total).toFixed(2);
  }

  actualizar() {
    this.cart = JSON.parse(localStorage.getItem('cartdata'));
    this.total = 0;
    this.contador = 0;
    for (var i = 0; i < this.cart.length; i++) {
      this.total += this.cart[i].cantidad * this.cart[i].price;
    }
    this.contador = this.cart.length;
    this.total2 = Number(this.total).toFixed(2);
  }

  openCatego = function (i) {
    var elemento = document.getElementById(i);
    var elemento2 = document.getElementById('titulo' + i);
    if (elemento.classList.contains('openn')) {
      elemento.classList.remove('openn');
      elemento2.classList.remove('open');
      elemento.classList.add('closee');
    } else {
      elemento.classList.add('openn');
      elemento.classList.remove('closee');
      elemento2.classList.add('open');
    }
  };

  openCatego2 = function () {
    var elemento = document.getElementById('fijo');
    var elemento2 = document.getElementById('TodosFijo');
    if (elemento.classList.contains('openn')) {
      elemento.classList.remove('openn');
      elemento2.classList.remove('open');
      elemento.classList.add('closee');
    } else {
      elemento.classList.add('openn');
      elemento.classList.remove('closee');
      elemento2.classList.add('open');
    }
  };

  confirmaEliminar(i){
    this.toastr.warning('¿Estas seguro de eliminarlo?', 'Toca para confirmar',{
      closeButton:true,
      progressBar:false,
      positionClass: 'toast-center-center',
      disableTimeOut:true
    })
      .onTap.subscribe(() => this.deleteProduct(i));
  }

}
