import { Component, OnInit } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ProductserviceService } from '../../productservice.service';
@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css'],
})
export class ProductoComponent implements OnInit {
  private baseURL = 'https://orvity.com/orv/';
  idP;
  producto: any = [];
  public cart = [];
  public total = 0;
  public contador = 0;
  public filterData: any = [];
  nombreEmpresa = '';
  cantidad = 1;
  img = "";
  imgPortada = "";

  constructor(
    public route: ActivatedRoute,
    private http: HttpClient,
    private productService: ProductserviceService
  ) {
    this.route.params.subscribe((parametros) => {
      this.idP = parametros['idP'];
      console.log(this.idP);
    });
  }

  ngOnInit(): void {
    this.obtenerProducto();

    this.cart = JSON.parse(localStorage.getItem('cartdata'));
    console.log('cart', this.cart);

    var previousProducts = JSON.parse(localStorage.getItem('products'));
    console.log('previousProducts', previousProducts);

    // var index = this.cart.findIndex(x => x.id == previousProducts[0].id);

    // console.log('index', index)
    // this.cart.splice(index, 1);
    // localStorage.setItem('cartdata',JSON.stringify(this.cart));
    // console.log('previodProduct', previousProducts)

    for (var i = 0; i < this.cart.length; i++) {
      this.total += this.cart[i].cantidad * this.cart[i].precio_producto;
    }
    this.contador = this.cart.length;
    // console.log(this.total);
    // this.deleteProduct();
    var name = JSON.parse(localStorage.getItem('infoEmpresa'));
    this.nombreEmpresa = name[0].nombre_users;
    this.img = name[0].img_perfil;
    this.imgPortada = name[0].img_portada;
    console.log(this.nombreEmpresa);
  }

  obtenerProducto() {
    const headers: any = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    const options: any = { caso: 1, idProduct: this.idP };
    const URL: any = this.baseURL + 'compras.php';
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.producto = respuesta;
        console.log(this.producto);
      });
  }

  addtoCart(id,cantidad) {
    this.filterData = this.productService.addtoCart(id, cantidad);
    // console.log(this.filterData)
    this.actualizar();
  }

  public cantidadProduct(event: any): void {
    console.log(this.cantidad);
  }

  deleteProduct(index) {
    this.cart = JSON.parse(localStorage.getItem('cartdata'));
    console.log('cart', this.cart);

    var previousProducts = JSON.parse(localStorage.getItem('products'));
    console.log('previousProducts', previousProducts);

    // var index = this.cart.findIndex((x) => x.id === previousProducts[0].id);

    console.log('index', index);
    this.cart.splice(index, 1);
    localStorage.setItem('cartdata', JSON.stringify(this.cart));

    this.total = 0;
    for (var i = 0; i < this.cart.length; i++) {
      this.total += this.cart[i].cantidad * this.cart[i].precio_producto;
    }
    this.contador = this.cart.length;
  }

  actualizar() {
    this.cart = JSON.parse(localStorage.getItem('cartdata'));
    this.total = 0;
    this.contador = 0;
    for (var i = 0; i < this.cart.length; i++) {
      this.total += this.cart[i].cantidad * this.cart[i].precio_producto;
    }
    this.contador = this.cart.length;
  }
}
