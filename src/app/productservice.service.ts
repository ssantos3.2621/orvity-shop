import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class ProductserviceService {
  constructor(private toastr: ToastrService) {}

  fetchProduct() {
    // localStorage.removeItem('cartdata');
    return JSON.parse(localStorage.getItem('products')) || []; //saved data of product into localStorage
  }

  //add products from home page cart
  addtoCart(id, cantidad) {
    //filtering selected data with incoming data from localstorage using id
    var fetchdata = this.fetchProduct().filter((m) => m.id == id);
    // console.log('Selected Product', fetchdata)
    var previousCartItems = JSON.parse(localStorage.getItem('cartdata')) || [];

    // console.log('previousItem',previousCartItems);
    // console.log(fetchdata[0].id);

    var addmore = previousCartItems.filter(
      (d) => d.id == fetchdata[0].id
    );
    var index = previousCartItems.findIndex(
      (d) => d.id == fetchdata[0].id
    );

    // console.log('Matheched',addmore);
    // console.log('INdex',index);
    if (addmore.length > 0) {
      // console.log(addmore.cantidad)
      var cartItem: any = {
        id: fetchdata[0].id,
        title: fetchdata[0].title,
        price: fetchdata[0].price,
        // "qty": addmore.quantity
        cantidad: cantidad,
        description: fetchdata[0].description,
        imageProduct: fetchdata[0].imageProduct,
      };
      previousCartItems[index] = cartItem;
      this.toastr.error('Ya esta agregado al carrito', fetchdata[0].title);
    } else {
      this.toastr.success('Se agrego al carrito', fetchdata[0].title);
      var cartItem: any = {
        id: fetchdata[0].id,
        title: fetchdata[0].title,
        price: fetchdata[0].price,
        // "qty": addmore.quantity
        cantidad: cantidad,
        description: fetchdata[0].description,
        imageProduct: fetchdata[0].imageProduct,
      };
      previousCartItems.push(cartItem);
    }

    localStorage.setItem('cartdata', JSON.stringify(previousCartItems));
    console.log(localStorage.getItem('cartdata'));
  }

  fetchRegister() {
    // localStorage.removeItem('registers')
    return JSON.parse(localStorage.getItem('registers')) || [];
  }
}
